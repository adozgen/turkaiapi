@component('mail::message')
# Tebrikler üyeliğiniz başarılı şekilde gerçekleşmiştir.

Teşekkürler,<br>
{{ config('app.name') }}
@endcomponent
