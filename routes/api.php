<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware("auth:sanctum")->group(function () {
     Route::get('/user', "UserController@show");
     Route::get("students", 'UserController@students');
     Route::get("userstudents", 'UserController@userStudents')->middleware("turkai");
     Route::put("user/{id}", 'UserController@update');
     Route::post("logout", 'AuthController@logout');
});


Route::post("login", 'AuthController@login');
Route::post("register", 'UserController@create');


