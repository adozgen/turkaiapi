<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface StudentRepositoryInterface
{
   public function all();
}
