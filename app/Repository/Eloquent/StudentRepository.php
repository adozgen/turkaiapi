<?php

namespace App\Repository\Eloquent;

use App\Student;
use App\Repository\StudentRepositoryInterface;
use Illuminate\Support\Collection;

class StudentRepository extends BaseRepository implements StudentRepositoryInterface
{
   public function __construct(Student $model)
   {
       parent::__construct($model);
   }


   public function all()
   {
       return $this->model->all();
   }
   public function findByCode($code): Collection {
       return $this->model->whereCode($code)->get();
   }

}
