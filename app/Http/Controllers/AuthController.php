<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

use App\Repository\UserRepositoryInterface;
use App\Http\Resources\User as UserResource;

class AuthController extends Controller
{
   private $userRepository;
   public function __construct(UserRepositoryInterface $userRepository)
   {
       $this->userRepository = $userRepository;
   }



    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $user = new UserResource($this->userRepository->findByEmail($request->email));
        if (!$user || !Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['Girilen bilgilerle eşleşen kayıt bulunamadı.'],
            ]);
        }
        return $user->createToken("browser")->plainTextToken;
    }

    public function logout(Request $request)
    {
        try {
             $request->user()->tokens()->delete();
        } catch (\Throwable $th) {
             return response()->json($th->getMessage());
        }
    }
}
