<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Student;
use Validator;
use App\Role;
use Illuminate\Support\Facades\Hash;
use App\Events\NewUserRegisteredEvent;
use Illuminate\Validation\ValidationException;

use App\Repository\UserRepositoryInterface;
use App\Repository\StudentRepositoryInterface;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\Student as StudentResource;

use Collection;

class UserController extends Controller

{
   private $userRepository;
   private $studentRepository;
   public function __construct(UserRepositoryInterface $userRepository,
                               StudentRepositoryInterface $studentRepository)
   {
       $this->userRepository = $userRepository;
       $this->studentRepository = $studentRepository;
   }
    public function userStudents(){
           try {
             $users =  $this->userRepository->with("students");
             $userStudents = UserResource::collection($users);
             return response()->json($userStudents, 200);
        } catch (\Throwable $th) {
            return response()->json(["message"=>$th->getMessage()]);
        }
    }

    public function students(Request $request){
           try {
             $userstudents =  $this->userRepository->withRelations($request->user()->id, "students");
             return StudentResource::collection($userstudents);
        } catch (\Throwable $th) {
            return response()->json(["message"=>$th->getMessage()]);
        }
    }



    public function show(Request $request){
        $user = new UserResource($request->user());
        return response()->json($user);
    }

    public function create(Request $request){
           try {
            $valid = Validator::make($request->all(), [
                'name' => 'required',
                'password' => 'required',
                'email' => 'required|unique:users,email',
                'code'=> 'required'
            ]);
            if ($valid->fails()) {
                return response()->json(['message' => $valid->errors()], 500);
            } else {
                $students = $this->studentRepository->findByCode($request->code);
                if($students->count() > 0){
                   $user = [
                          "name"=>$request->name,
                          "password"=>$request->password, //User modelinde mutators özelliğiyle hashlendi
                          "email"=>$request->email,
                          "code"=>$request->code
                    ];
                   $students = $this->studentRepository->findByCode($request->code);
                   $this->userRepository->createWithStudents($user, $students);
                   return response()->json(['message' => "Kayıt İşleminiz başarıyla gerçekleşmiştir."], 200);
                } else {
                    return response()->json(['message' => "Bu koda sahip sistemde öğrencimiz yoktur."], 500);
                }
            }
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }

    }

    public function update($id, Request $request)
    {
        try {
            $user = $this->userRepository->find($id);
             if (!$user || !Hash::check($request->currentpassword, $user->password)) {
                    return response()->json(["message"=>"Mevcut parolanızı yanlış girdiniz."], 500);
             }
            $this->userRepository->update($id, $request);
            return response()->json(["message"=>"Parolanız başarılı bir şekilde değiştirildi."], 200);
        } catch (Exception $e) {
           return response()->json(['message' => $e->getMessage()], $e->getStatus());
        }
    }
}
