<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Student;
use Validator;
use App\Role;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Mail\WelcomeNewUserMail;
use App\Events\NewUserRegisteredEvent;

class TestController extends Controller
{
    public function test(){
        try {
             $user = new User();
                $user->name = "test";
                $user->email = "adesdjfnjds@gmail.com";
                $user->password = Hash::make("deneme");
                $user->save();

                event(new NewUserRegisteredEvent($user));
        } catch (\Throwable $th) {
            return response()->json($th->getMessage());
        }
    }

}
