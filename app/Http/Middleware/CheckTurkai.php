<?php

namespace App\Http\Middleware;

use Closure;

class CheckTurkai
{
    public function handle($request, Closure $next)
    {
        if($request->user()->email != 'turkai@turkai.com'){
                   return response()->json(["message"=>"Yekiniz dışında bir alana erişmeye çalışıyorsunuz. Bu alana turkai@turkai.com email hesabına sahip kullanıcılar girebilir."], 500);


        }
        return $next($request);


    }
}
