<?php

use Illuminate\Database\Seeder;
use App\Student;


class StudentSeeder extends Seeder
{
    public function run()
    {
    $faker = Faker\Factory::create('tr_TR');
    for($i=0; $i<10; $i++) {
         Student::create([
           'name' => $faker->name,
           'code' => Str::random(10),
        ]);
    }
    }
}
